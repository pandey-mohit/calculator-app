/**
 * Created by mohit pandey on 27/12/14.
 */


(function(){

    // create calculator method/class
    calculator = function(){};

    // defining calculator prototype
    calculator.prototype = {
        // contains equation
        equation: '',
        // memory contains a number which can be retrieved later
        memory: '',
        // operator string, used to check whether inserted text in number or operator
        operator: '/*+-',
        // flag to check whether calculated result is saved in equation or not
        isResultCalculated: false,
        // method to validate text insertion in equation
        insert: function(text){

            // check for maximum limit of 20 numbers
            if(this.equation.replace(/[+-\/\*() ]/gi,'').length == 20)
                return alert('Maximum limit exceeded.');

            // get last character
            var lastCharacter = this.getLastCharacter();

            // check for duplicacy or repeated operators
            if(this.operator.indexOf(text) !== -1 && this.operator.indexOf(lastCharacter) !== -1 )
                return false;

            // check for duplicacy of dot in a single number
            if(text == '.') {
                //var expArr = this.equation.replace(/ /g,'').replace(/[-+\/\*]/g, ' ').split(' ');
                var expArr = this.equation.replace(/\s*[-+\/\*]\s*/g, ' ').split(' ');
                if(expArr[expArr.length - 1].search(/\./) !== -1)
                    return false;
            }

            // if result is calculated then replace equation with new text
            if(this.isResultCalculated){
                this.isResultCalculated = false;
                if(this.operator.indexOf(text) !== -1)
                    this.insertTextToEquation(text);
                else
                    this.equation = text;
            }
            else {
                this.insertTextToEquation(text);
            }
        },
        // method to insert text to equation
        insertTextToEquation: function(text){

            // return if an operator is inserted in an empty equation
            if(this.equation === '' && ('/*+-').indexOf(text) !== -1)
                return;

            if(('/*+-').indexOf(text) !== -1){
                if(this.equation.match(/[-+\/\*]/g) !== null)
                    this.equation = '('+ this.equation + ')';
                text = ' ' + text + ' ';
            }

            this.equation += text;

        },
        // method to get last character in equation
        getLastCharacter: function(){
            var eq = this.equation.trim();
            return eq.substr(eq.length - 1);
        },
        // method to return pre compiled result
        result: function(){
            return this.eval();
        },
        // method to return result which is saved in equation
        saveResult: function(){
            this.isResultCalculated = true;

            this.equation = this.eval();
            return this.equation;
        },
        // method to evaluate result from equation
        eval: function(){
            if(this.equation === '')
                return '';

            var lastCharacter = this.getLastCharacter();
            var eq = this.equation.trim();
            if(this.operator.indexOf(lastCharacter) !== -1){
                eq = eq.substr(0, eq.length - 1).trim();
            }

            var result = '';
            try {
                result = (new Function('return ' + eq))() + '';
            } catch(err){
                console.log('error: ' + err);
            }
            return result;
        },
        // method to get memory
        getMemory: function(){
            this.equation = this.memory;
            return this.memory;
        },
        // method to set memory
        setMemory: function(){
            //var exp = this.equation.replace(/ /g,'').replace(/[+-\/\*]/g, ' ').trim().split(' ');
            var exp = this.equation.replace(/\s*[-+\/\*]\s*/g, ' ').trim().split(' ');
            this.memory = exp[exp.length - 1];
        },
        // method to clear equation and memory
        clear: function(){
            this.equation = '';
            this.memory = '';
        },
        // method to get equation
        getEquation: function(){
            return this.equation;
        },
        // method to remove last number or operator from equation
        removeLastNumber: function(){
            this.equation = this.equation.trim();
            var lastCharacter = this.getLastCharacter();

            // remove parenthesis if last word character is operator
            if(this.operator.indexOf(lastCharacter) !== -1){
                if(this.equation.indexOf('(') !== -1)
                    this.equation = this.equation.substr(1, this.equation.length - 4).trim();
                else
                    this.equation = this.equation.substr(0, this.equation.length - 1).trim();
            } else {
                this.equation = this.equation.substr(0, this.equation.length - 1);
            }

        }
    }

})();