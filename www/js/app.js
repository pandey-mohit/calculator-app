/**
 * Created by mohit pandey on 27/12/14.
 */


(function(){

    // method to detect a touch device
    function isTouchSupported(){
        return 'ontouchstart' in document.documentElement;
    }

    // method resize DOM to fit to screen
    function resizeDOM() {
        // get window height
        var winHeight = $(window).height();

        // set table height to 100%
        $('table').height(winHeight);

    };


    /* bind element method once DOM is ready */
    $(document).ready(function(){
        // resize DOM
        resizeDOM();

        // initialize calculator
        var calc = new calculator();

        // if its a touch device then use touchend event to detect click
        var clickEvent = isTouchSupported() ? 'touchend' : 'click';

        // method to handle click event
        $('td').on(clickEvent, function(e){
            if(!$(this).hasClass('prevent-click')){
                var text  = $(this).text();

                switch(text){
                    case 'c':
                        calc.clear();
                        break;
                    case 'mr':
                        calc.getMemory();
                        break;
                    case 'm+':
                        calc.setMemory();
                        break;
                    case '<':
                        calc.removeLastNumber();
                        break;
                    case '=':
                        calc.saveResult();
                        break;
                    default:
                        calc.insert(text);
                }

                // show equation in result window
                $('#result').html(calc.getEquation());

                // show precompiled result
                var compileResult = calc.result();
                compileResult = compileResult === '' ? '&nbsp;' : compileResult;
                $('#compile-result').html(compileResult);

            }
        })

    });

    // window resize method to handle resizing of window
    $(window).resize(resizeDOM);

})();

